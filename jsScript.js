
const slides = document.querySelector('.slideShow');
const slideImages = document.querySelectorAll('.slideShow img');
const dots = document.querySelectorAll('.dot');

const prevBtn = document.querySelector('#prev');
const nextBtn = document.querySelector('#next');

let imgCurrent = 1;
const size = slideImages[0].clientWidth + 30; //30 from margin img5 + left img1 

//first set view
slides.style.transform = 'translateX(' + (-size * imgCurrent) + 'px)';
dots[imgCurrent-1].style.background = '#F16E03';

nextBtn.addEventListener('click', ()=>{
    if(imgCurrent >= slideImages.length - 1 ) return;
    slides.style.transition = "transform 0.4s ease-in-out";
    imgCurrent++;
    slides.style.transform = 'translateX(' + (-size * imgCurrent) + 'px)';
});

prevBtn.addEventListener('click', ()=>{
    if(imgCurrent <= 0 ) return;
    slides.style.transition = "transform 0.4s ease-in-out";
    imgCurrent--;
    slides.style.transform = 'translateX(' + (-size * imgCurrent) + 'px)';
});

//slide reposition
slides.addEventListener('transitionend', ()=> {
    if(slideImages[imgCurrent].id === 'lastImg') {
        console.log("last");
        slides.style.transition = 'none';
        imgCurrent = slideImages.length - 2;
        slides.style.transform = 'translateX(' + (-size * imgCurrent) + 'px)';
    }
    if(slideImages[imgCurrent].id === 'firstImg') {
        slides.style.transition = 'none';
        imgCurrent = slideImages.length - imgCurrent;
        slides.style.transform = 'translateX(' + (-size * imgCurrent) + 'px)';
    }
});

//dot click action
for(let i = 0; i < dots.length; i++) {
    dots[i].addEventListener('click', ()=>{
        imgCurrent = i + 1 ;
        slides.style.transition = "transform 0.4s ease-in-out";
        slides.style.transform = 'translateX(' + (-size * imgCurrent) + 'px)';
        dots[i].style.background = '#F16E03';

        //reset dot style
        for(let j = 0 ; j < dots.length ; j++) {
            if(j != i) {
                dots[j].style.background = '#F6E0B7';
            }
        }
    });
}